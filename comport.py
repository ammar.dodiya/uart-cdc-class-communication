#!/usr/bin/env python3


#panid 0xabcd
#ifconfig up
#thread start
#ipaddr
#coap start
#coap post ipaddr led con text 
#coap response from ipaddr

###########################################################
# openthread cli commands
###########################################################
cmd_factoryreset = "factoryreset\r\n"
cmd_panid = "panid 0xabcd\r\n"
cmd_ifconfig = "ifconfig up\r\n"
cmd_thread_start = "thread start\r\n"
cmd_ipaddr = "ipaddr\r\n"
cmd_coap_start = "coap start\r\n"
cmd_coap_post = "coap post "

ot1_ipaddr = ""
ot2_ipaddr = ""

lpc1_port = ""
lpc2_port = ""
ot1_port = ""
ot2_port = ""

lpc1_data = ""
lpc2_data = ""
ot1_data = ""
ot2_data = ""
data1_packet = ""
data2_packet = ""

print("Starting COMPORT script.")

import sys
import argparse
import serial
import time
import datetime

###########################################################
# Provide arguments necassary for running this script where 
# LPC1 USB CDC Class port number toward LPC55S69 board - ex /dev/ttyACM1
# LPC2 USB CDC Class port number toward LPC55S69 board - ex /dev/ttyACM2
# OT1  USB port number toward openthread dongle - ex /dev/ttyUSB0
# OT2  USB port number toward openthread dongle - ex /dev/ttyUSB1
###########################################################
parser = argparse.ArgumentParser()
parser.add_argument('--LPC1', type=str ,help='Pass LPC Board 1', required=True)
parser.add_argument('--LPC2', type=str,help='Pass LPC Board 2', required=True)
parser.add_argument('--OT1', type=str,help='Pass openthread dongle 1', required=True)
parser.add_argument('--OT2', type=str,help='Pass openthread dongle 2', required=True)


# Parse argument passsed while starting script 
args = parser.parse_args()


###########################################################
# Configure USB port to run as serial terminal as standard UART
# Open port so script can start writing on that port
# portname 	- parameter passed as string port name like /dev/ttyUSB1
# return 	- configured serial port instance
###########################################################
def open_port(portname):
	print("Configuring -" , portname)
	# Baudrate - 115200
	ser = serial.Serial(portname,115200)
	# Transfer size - 8 bit
	ser.bytesize=8
	# Timeout should be 0,it should return as fast as possible
	ser.timeout=0
	# set one stop bit
	ser.stopbits=serial.STOPBITS_ONE
	# close port if previously it was open
	ser.close()
	# open port so it can start transferring data to it
	ser.open()
	return ser
    
	
###########################################################
# Function used for writing on port and wait till expected data received
# port		- port instance create from open_port function
# msg_data  - data which needs to be sent to other device
# wait_msg 	- data until which this loop must execute
# minDelay  - delay between each data read
# get_data  - flag which controls wether received data should be returned or not
# return    - if get_data is true that data received should be returned for further processing
###########################################################
def write_data_on_port(port,msg_data,wait_msg,minDelay,get_data):
	localText = ""
	ipaddr = ""
	newLine = "\r\n"
	# reset input buffer if rpreviously data received wasent flushed
	port.reset_input_buffer()
	# send new line data 
	port.write(newLine.encode())
	# send actual data which needs to be transmitted
	port.write(msg_data.encode())
	while True:
		# sleep system as described time passed from function argument
		time.sleep(minDelay)
		# read line from port as every text ends with newline character
		localText = port.readline()
		# if data required to be returned than update local variable
		if get_data == True and b':' in localText:
			#print ("get_data",ipaddr, localText) 
			ipaddr = localText
		#print ("message_data",msg_data, localText)
		# check if waiting data exist in received text
		if wait_msg.encode() in localText:
			print ("PORT : ", port.name," CMD : ",msg_data.rstrip()," DATA : ",localText," IP : " ,ipaddr)
			return ipaddr

	return

###########################################################
# initialize openthread dongle procedure till network started 
# port1		- LPC1 port instance
# port2		- LPC2 port instance
# ot1_addr	- OT1 port instance
# ot2_addr	- OT2 port instance
###########################################################
def initialize_openthread(port1,port2,ot1_addr,ot2_addr):
	global ot1_ipaddr
	global ot2_ipaddr
	# write device reset command on both port and wait till it resets
	write_data_on_port(port1,cmd_factoryreset,"Device Extended",0.1,False)
	write_data_on_port(port2,cmd_factoryreset,"Device Extended",0.1,False)

	# write panid command to both port
	write_data_on_port(port1,cmd_panid,"Source PANID",0.1,False)
	write_data_on_port(port2,cmd_panid,"Source PANID",0.1,False)

	# enable network configuration
	write_data_on_port(port1,cmd_ifconfig,"Done",0.1,False)
	write_data_on_port(port2,cmd_ifconfig,"Done",0.1,False)

	# start thread network
	write_data_on_port(port1,cmd_thread_start,"role",0.1,False)
	write_data_on_port(port2,cmd_thread_start,"role",0.1,False)

	# start coap security 
	write_data_on_port(port1,cmd_coap_start,"Done",0.1,False)
	write_data_on_port(port2,cmd_coap_start,"Done",0.1,False)

	# get ipaddress of both side openthread dongle
	ot2_addr = write_data_on_port(port1,cmd_ipaddr,"Done",0.1,True)
	ot1_addr = write_data_on_port(port2,cmd_ipaddr,"Done",0.1,True)
	
	# update global ipaddress variable of both sides
	ot2_ipaddr = ot1_addr.decode('UTF-8').rstrip()
	ot1_ipaddr = ot2_addr.decode('UTF-8').rstrip()
#	print("Addr : ",ot1_addr,ot2_addr)
#	print("IP : ",ot1_ipaddr,ot2_ipaddr)

	return


###########################################################
# Parse received message and return parsed text
# received_data	- received message on port
# return		- parsed text
###########################################################
def parse_response(received_data):
	valid_data = ""
	#print("len",len(received_data.decode().split("Received Data : ")),received_data.decode())
	# check if parsed text length is greater than 1 character than return parsed data
	if len(received_data.decode().split("Received Data : ")) > 1:
		valid_data = received_data.decode().split("Received Data : ")[1]
		return valid_data


###########################################################
# calculate threoughtput
# length		- total number of bytes in packet
# microseconds	- total time required to transmit this packet
# return		- throughtput in bytes per second
###########################################################
def get_throughput(length,microseconds):
	bytes_per_second=(length * 1000000)/(microseconds)
	return bytes_per_second


if __name__ == "__main__":
	
	# configure and open all the usb ports
	lpc1_port = open_port(args.LPC1)
	lpc2_port = open_port(args.LPC2)
	ot1_port = open_port(args.OT1)
	ot2_port = open_port(args.OT2)
	
	# initialize openthread dongle 
	initialize_openthread(ot1_port,ot2_port,ot1_ipaddr,ot2_ipaddr)
    
	print("Initialization Completed -> ",ot1_ipaddr," -> ",ot2_ipaddr)    
	while True:
		# sleep execution for minimum amount of time
		time.sleep(0.01)

		# read line from all the ports
		lpc1_data = lpc1_port.readline()
		lpc2_data = lpc2_port.readline()
		ot1_data = ot1_port.readline()
		ot2_data = ot2_port.readline()
		#print("Text -> ",data1_packet,ot2_ipaddr,lpc1_data)
		
		# check lpc1 data contains new line character than process further
		if b'\r\n' in lpc1_data:
			# Format CLI command to transmit data
			data1_packet = str(cmd_coap_post) + str(ot2_ipaddr.rstrip()) + " led con " + str(lpc1_data.decode())
			print("LPC1 -> ",data1_packet.encode())
			# write formatted command to openthread dongle port
			ot1_port.write(data1_packet.encode())
			# get current timestamp
			lpc1_time = datetime.datetime.now()
	
		# check lpc2 data contains new line character than process further
		if b'\r\n' in lpc2_data:
			# Format CLI command to transmit data
			data2_packet = str(cmd_coap_post) + str(ot1_ipaddr.rstrip()) + " led con " + str(lpc2_data.decode())
			print("LPC2 -> ",data2_packet.encode())
			# write formatted command to openthread dongle port
			ot2_port.write(data2_packet.encode())
			# get current timestamp
			lpc2_time = datetime.datetime.now()
		
		# check openthread1 data contains new line character than process further
		if b'\r\n' in ot1_data:
			# parse data and process further only if received data is not null or empty
			if parse_response(ot1_data) is not None:
				# send received data to lpc1 port
				lpc1_port.write(parse_response(ot1_data).encode())
				# get current timestamp
				ot1_time = datetime.datetime.now()
				# get difference between two timestamp
				lpc2_ot1_time = ot1_time - lpc2_time
				# print latency and throughput
				print("LPC2 -> OT1 : Latency :",lpc2_ot1_time.microseconds," uS"," -> Throughput : ",format(get_throughput(len(ot1_data),lpc2_ot1_time.microseconds),'.2f')," bytes/sec")
				# reset variable
				lpc2_time = None
				ot1_time = None
				lpc2_ot1_time = None

		# check openthread2 data contains new line character than process further
		if b'\r\n' in ot2_data:
			# parse data and process further only if received data is not null or empty
			if parse_response(ot2_data) is not None:
				# send received data to lpc2 port
				lpc2_port.write(parse_response(ot2_data).encode())
				# get current timestamp
				ot2_time = datetime.datetime.now()
				# get difference between two timestamp
				lpc1_ot2_time = ot2_time - lpc1_time
				# print latency and throughput
				print("LPC1 -> OT2 : Latency :",lpc1_ot2_time.microseconds," uS"," -> Throughput : ",format(get_throughput(len(ot2_data),lpc1_ot2_time.microseconds),'.2f')," bytes/sec")
				# reset variable
				lpc1_time = None
				ot2_time = None
				lpc1_ot2_time = None
			#lpc2_port.write(ot2_data)
	
	

