################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../drivers/fsl_clock.c \
../drivers/fsl_common.c \
../drivers/fsl_flexcomm.c \
../drivers/fsl_gpio.c \
../drivers/fsl_reset.c \
../drivers/fsl_usart.c 

OBJS += \
./drivers/fsl_clock.o \
./drivers/fsl_common.o \
./drivers/fsl_flexcomm.o \
./drivers/fsl_gpio.o \
./drivers/fsl_reset.o \
./drivers/fsl_usart.o 

C_DEPS += \
./drivers/fsl_clock.d \
./drivers/fsl_common.d \
./drivers/fsl_flexcomm.d \
./drivers/fsl_gpio.d \
./drivers/fsl_reset.d \
./drivers/fsl_usart.d 


# Each subdirectory must supply rules for building sources it contributes
drivers/%.o: ../drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -D__REDLIB__ -DHOST_ECHO -DCPU_LPC55S69JBD100 -DCPU_LPC55S69JBD100_cm33 -DCPU_LPC55S69JBD100_cm33_core0=1 -D_DEBUG=1 -DDEBUG_CONSOLE_RX_ENABLE=0 -DDEBUG_CONSOLE_TRANSFER_NON_BLOCKING -DDEBUG_CONSOLE_RECEIVE_BUFFER_LEN=16 -DDEBUG_CONSOLE_TRANSMIT_BUFFER_LEN=64 -DUSB_STACK_FREERTOS -DUSB_STACK_FREERTOS_HEAP_SIZE=16384 -DUSB_STACK_USE_DEDICATED_RAM=1 -DFSL_RTOS_FREE_RTOS -DFSL_OSA_BM_TASK_ENABLE=0 -DFSL_OSA_BM_TIMER_CONFIG=0 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DCR_PRINTF_CHAR -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/board" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/source" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/usb/host" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/usb/include" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/freertos/freertos_kernel/portable/GCC/ARM_CM33_NTZ/non_secure" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/freertos/freertos_kernel/include" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/usb/host/class" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/drivers" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/LPC55S69/drivers" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/device" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/startup" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/usb/phy" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/utilities" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/component/uart" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/component/serial_manager" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/component/lists" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/component/osa" -I"/home/ammar/Embed/Pre-thesis/Project_example/lpcxpresso55s69_host_cdc_freertos/CMSIS" -O0 -fno-common -g3 -mno-unaligned-access -mcpu=cortex-m33 -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m33 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


